# conde-nast-assignment

NGINX proxy for our hello-world django app

## How to use?


### Environment Variables

* `LISTEN_PORT` - Port to listen (default: 8000)
* `APP_HOST` - Hostname of the app to forward request to (default: `app`)
* `APP_PORT` - Port of the app to forward request to (default: `9000`)

